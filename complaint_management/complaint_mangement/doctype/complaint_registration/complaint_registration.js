// Copyright (c) 2017, indictrans and contributors
// For license information, please see license.txt

cur_frm.add_fetch('contact', 'phone', 'phone');
cur_frm.add_fetch('contact', 'mobile_no', 'mobile_no');
cur_frm.add_fetch('address', 'email_id', 'address_email_id');
cur_frm.add_fetch('item_code','item_name','item_name')
frappe.ui.form.on('Complaint Registration', {
	refresh: function(frm) {

	},
	onload: function(frm) {
		if (!frm.doc.date) {
			frm.set_value("opening_date", get_today());
		}
	}	
});


cur_frm.fields_dict['complaint_registration_service_details'].grid.get_field('item_code').get_query = function(doc, cdt, cdn) {
	var d = locals[cdt][cdn]
	return{
		filters:[
			['Item', 'item_group', '=', d.item_group]
		]
	}
}